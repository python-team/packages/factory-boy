Source: factory-boy
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Brian May <bam@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 pybuild-plugin-pyproject,
 python3-all,
 python3-setuptools,
Build-Depends-Indep:
 python-mongoengine-doc <!nodoc>,
 python-sqlalchemy-doc <!nodoc>,
 python3-doc <!nodoc>,
 python3-django <!nocheck>,
 python3-fake-factory <!nocheck> <!nodoc>,
 python3-mongoengine <!nocheck>,
 python3-mongomock <!nocheck>,
 python3-pytest <!nocheck>,
 python3-sqlalchemy <!nocheck>,
 python3-sphinx  <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/factory-boy
Vcs-Git: https://salsa.debian.org/python-team/packages/factory-boy.git
Homepage: https://github.com/FactoryBoy/factory_boy
Rules-Requires-Root: no

Package: python-factory-boy-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: Test fixtures replacement based on factory_girl for Ruby (Documentation)
 factory_boy is a fixtures replacement based on thoughtbot's factory_girl. Like
 factory_girl it has a straightforward definition syntax, support for multiple
 build strategies (saved instances, unsaved instances, attribute dicts, and
 stubbed objects), and support for multiple factories for the same class,
 including factory  inheritance.
 .
 Django support is included, and support for other ORMs can be easily added.
 .
 This package provides the documentation.

Package: python3-factory-boy
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-factory-boy-doc,
Description: Test fixtures replacement based on factory_girl for Ruby (Python 3)
 factory_boy is a fixtures replacement based on thoughtbot's factory_girl. Like
 factory_girl it has a straightforward definition syntax, support for multiple
 build strategies (saved instances, unsaved instances, attribute dicts, and
 stubbed objects), and support for multiple factories for the same class,
 including factory  inheritance.
 .
 Django support is included, and support for other ORMs can be easily added.
 .
 This package provides the Python 3.x module.
